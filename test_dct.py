import numpy as np
import math
import time
from scipy.fft import dctn, dct
import os
import csv


def my_dct(vector):
    n = len(vector)

    w = np.zeros(n)

    alpha_zero = 1 / math.sqrt(n)
    alpha_not_zero = math.sqrt(2 / n)

    for k in range(n):
        if k == 0:
            alpha = alpha_zero
        else:
            alpha = alpha_not_zero
        s = 0
        for i in range(n):
            s += vector[i] * np.cos(np.pi * k * ((2 * i +1) / (2 * n)))
        w[k] = alpha * s
    
    return w

def my_dct2(matrix):
    n, m = matrix.shape
   
    A = np.zeros((n, m), dtype = "float")
    
    for k in range(m):
        A[:, k] = my_dct(matrix[:, k])

    print("Matrice " + str(m) + ": colonne finite!")
    
    for l in range(n):
        A[l, :] = my_dct(A[l, :])
    
    return A

def test_dct():
    v = np.array([231, 32, 233, 161, 24, 71, 140, 245])

    print("Testing scipy dct:")
    print(dct(v, norm='ortho'))

    print("\nTesting my dct:")
    print(my_dct(v))

def test_dct2():
    m = np.array([[231,  32, 233, 161,  24,  71, 140, 245],
                [247,  40, 248, 245, 124, 204,  36, 107],
                [234, 202, 245, 167,   9, 217, 239, 173],
                [193, 190, 100, 167,  43, 180,   8,  70],
                [ 11,  24, 210, 177,  81, 243,   8, 112],
                [ 97, 195, 203,  47, 125, 114, 165, 181],
                [193,  70, 174, 167,  41,  30, 127, 245],
                [ 87, 149,  57, 192,  65, 129, 178, 228]])
    
    print("Testing scipy dct2:")
    print(dctn(m, norm='ortho'))

    print("\nTesting my dct2:")
    print(my_dct2(m))

def perf_test(n, num_iteration):
    results = [] # n, time_scipy, time_my, delta: my-scipy
    
    dir_path = os.path.dirname(os.path.abspath(__file__))
    with open(dir_path + '/Performance_test.csv', 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(["n", "Scipy time", "My dct2 time", "Delta (My-Scipy)"])
    
    for i in range(num_iteration):
        matrix = np.random.randint(0, 300, size=(n, n))
        print("Matrice: " + str(n))
        
        tic = time.perf_counter()
        dctn(matrix, norm="ortho")
        toc = time.perf_counter()
        scipy_time = toc - tic
        print("Scipy time: " + str(scipy_time))

        tic = time.perf_counter()
        # my_dct2(matrix)
        toc = time.perf_counter()
        my_time = toc - tic
        print("My dct time: " + str(my_time))
        
        result = [n, scipy_time, my_time, my_time - scipy_time]
        with open(dir_path + '/Performance_test.csv', 'a', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(result)

        
        print(result)
        results.append(result)
        n = n * 2
        # n = n + 100
        i += 1

    
    # with open(dir_path + '/Performance_test.csv', 'w', encoding='UTF8', newline='') as f:
    #     writer = csv.writer(f)
    #     # write the header
    #     writer.writerow(["n", "Scipy time", "My dct2 time", "Delta (My-Scipy)"])
    #     writer.writerows(results)


def main():
    # test_dct()
    # print()
    # test_dct2()
    perf_test(600, 7)



if __name__ == '__main__':
    main()