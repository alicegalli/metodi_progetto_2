import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk, ImageStat
import numpy as np
import scipy.fft as fft
import os



# Funzione per selezionare un'immagine dal filesystem
def select_image():
    global max_f, max_d, image
    file_path = filedialog.askopenfilename(filetypes=[('Images', '*.bmp')])

    # Verifica se il file è stato selezionato
    if file_path:

        image = Image.open(file_path)

        max_f = min(image.height, image.width)
        max_d = 2 * max_f - 2
        f_label.config(text=("Dimensione finestrelle (F): \n (max value: " + str(max_f) + ")"))
        d_label.config(text=("Soglia di taglio (d): \n (max value: " + str(max_d) + ")"))
        
        # Verifica se l'immagine è in scala di grigi, altrimenti la converte
        stat = ImageStat.Stat(image.convert("RGB"))
        # Verifica che la media dei valori sia uguale a qualunque elemento 
        if sum(stat.sum)/3 != stat.sum[0]: # Verifica se l'immagine non sia in scala di grigi
            image = image.convert("L") # L'immagine è a colori, la converte in greyscale

        # Mostra l'immagine nella GUI
        image.thumbnail((500, 500))  # Ridimensiona l'immagine per adattarla alla GUI
        image_tk = ImageTk.PhotoImage(image)

        if os.path.getsize(file_path) / 1024 >= 1024: # è in MB
            memory_size = str(np.round(os.path.getsize(file_path) / (1024 * 1024), 2)) + " MB"
        else:
            memory_size = str(np.round(os.path.getsize(file_path) / 1024, 2)) + " KB"
        image_label.configure(image=image_tk, text=memory_size, compound="top")
        image_label.image = image_tk

        window.geometry('%dx%d' % (1200, 750))
        

        process_button.config(state=tk.NORMAL)
        process_button.config(command=lambda: process_image(file_path))
        

    
def process_image(file_path):
    global f, d
    list_path =os.path.split(os.path.abspath(file_path))
    dir_path = list_path[0]
    file_name = (list_path[1].split(".bmp"))[0]
    # Verifica dei valori inseriti per f e d, 
    # se sono nulli oppure non sono nel range possible in base alle dimensioni dell'immagine scelta, 
    # allora li setta al massimo possibile, altrimenti li lascia come sono stati inseriti
    if(f_entry.get() == ""  or int(f_entry.get()) not in range (1, max_f)):
        f = max_f
        f_entry_text.set(str(f))
    else:
        f = int(f_entry.get())
    
    if(d_entry.get() == ""  or int(d_entry.get()) not in range (1, max_d)):
        d = max_d
        d_entry_text.set(str(d))
    else:
        d = int(d_entry.get())
    
    # compressed_file_path = dir_path + "/" + file_name + "_compressed_f" + str(f) + "-d" + str(d) + ".jpeg"

    compressed_file_path = dir_path + "/CompressedImages/" + file_name + "_f" + str(f) + "-d" + str(d) + "_compressed.jpeg"

    with open(file_path, 'rb') as img:
        # se RGB, convertila
        image_greyscale = Image.open(img).convert('L')
    image_matrix = np.array(image_greyscale)

    # Recupera altezza e larghezza dell'immagine
    height = image_matrix.shape[0]
    width = image_matrix.shape[1]
    

    # Se l'altezza non è multiplo di f, si tronca al maggior valore multiplo di f
    if (height % f != 0):
        height = height - (height % f)

    # Se la larghezza non è multiplo di f, si tronca al maggior valore multiplo di f
    if (width % f != 0):
        width = width - (width % f)

    # Comprime l'immagine in base ad altezza e larghezza troncate
    image_matrix_truncated = image_matrix[0:height , 0:width]
    height = image_matrix_truncated.shape[0]
    width = image_matrix_truncated.shape[1]

    # Applica la DCT2 a tutti i blocchi dell'immagine
    blocks = apply_dct2(height, width, image_matrix_truncated)
    
    # Elimina le frequenze oltre la soglia d nei blocchi
    for i in range(len(blocks)):
        for k in range(f):
            for l in range(f):
                if k + l >= d:
                    blocks[i][k][l] = 0.0

    # Applica l'IDCT2 inversa ai blocchi modificati
    modified_blocks = apply_inverse_dct(height, width, blocks)
    
    # Ricomponi l'immagine unendo i blocchi
    output_image = merge_blocks(height, width, modified_blocks)
    

    # Mostra l'immagine compressa nella GUI
    output_image.thumbnail((500, 500))
    compressed_img_tk = ImageTk.PhotoImage(output_image)
    compressed_image_label.configure(image=compressed_img_tk)
    compressed_image_label.image = compressed_img_tk
    # output_image = output_image.save(compressed_file_path)

    # if os.path.getsize(compressed_file_path) / 1024 >= 1024: # è in MB
    #     compressed_memory_size = str(np.round(os.path.getsize(compressed_file_path) / (1024 * 1024), 2)) + " MB"
    # else:
    #     compressed_memory_size = str(np.round(os.path.getsize(compressed_file_path) / 1024, 2)) + " KB"
    compressed_image_label.configure(text=" ", compound="top")



# Funzione per applicare la DCT2 a tutti i blocchi dell'immagine
def apply_dct2(height, width, image_matrix_truncated):
    blocks = []

    # Dividi l'immagine in blocchi quadrati di dimensione F x F
    for i in range(0, height, f):
        for j in range(0, width, f):
            block =  image_matrix_truncated[i:i+f, j:j+f]
            block = np.array(block).reshape(f, f)
            block = dct2(block)
            blocks.append(block)

    # for y in range(0, height, f):
    #     for x in range(0, width, f):
    #         block = image.crop((x, y, x + f, y + f))
    #         block = np.array(block, dtype=np.float64)
            
    #         block = dct2(block)

    #         blocks.append(block)

    # for i in range(len(blocks)):
    #     blocks[i] = dct2(blocks[i])

    return blocks


# Funzione per eseguire la DCT2 su un blocco
def dct2(block):
    return fft.dct(fft.dct(block.T, norm='ortho').T, norm='ortho')


# Funzione per applicare l'IDCT2 inversa ai blocchi modificati
def apply_inverse_dct(height, width, blocks):
    images = [] # corrisponde al compressed_blocks

    # Esegui l'IDCT2 inversa su ciascun blocco
    for block in blocks:
        block = idct2(block)

        # Arrotonda i valori al più vicino intero
        block = np.round(block)
        # Metti a zero i valori negativi e a 255 quelli maggiori di 255
        block = np.clip(block, 0, 255)

        # Converti il blocco in un array di byte (uint8)
        # block = block.astype(np.uint8)

        images.append(block)

    return images


# Funzione per eseguire l'IDCT2 su un blocco
def idct2(block):
    return fft.idct(fft.idct(block.T, norm='ortho').T, norm='ortho')



def merge_blocks(height, width, blocks):
    compressed_matrix = np.empty((height, width))
    compressed_image = Image.new('L', (width, height))

    # Metti insieme i blocchi nell'ordine corretto
    k = 0
    for i in range(0, height, f):
        for j in range(0, width, f):
            compressed_matrix[i:i+f, j:j+f] = blocks[k]
            k += 1
    
    compressed_image = Image.fromarray(compressed_matrix.astype(np.uint8))

    return compressed_image



# Crea una finestra di GUI
window = tk.Tk()
window.title("Metodi del Calcolo Scientifico - Progetto 2")
width = 1000
height = 750

screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()

alignstr = '%dx%d+%d+%d' % (width, height, (screen_width - width) / 2, (screen_height - height) / 2)
window.geometry(alignstr)
window.resizable(width=True, height=True)

# window.eval('tk::PlaceWindow . center')

# Aggiungi un pulsante per selezionare un'immagine
select_image_button = tk.Button(window, text="Scegli immagine", command=select_image)
select_image_button.pack()

# Frame per contenere le immagini
image_frame = tk.Frame(window)
image_frame.pack()

# Visualizzazione dell'immagine originale
image_label = tk.Label(image_frame)
image_label.grid(row=0, column=0, padx=10, pady=10)

# Visualizzazione dell'immagine compressa
compressed_image_label = tk.Label(image_frame)
compressed_image_label.grid(row=0, column=1, padx=10, pady=10)

# Aggiungi un'etichetta e una casella di testo per l'input di F
f_label = tk.Label(window, text="Dimensione finestrelle (F):")
f_label.pack()
f_entry_text = tk.StringVar()
f_entry = tk.Entry(window, textvariable=f_entry_text)
f_entry.pack()

# Aggiungi un'etichetta e una casella di testo per l'input di d
d_label = tk.Label(window, text="Soglia di taglio (d):")
d_label.pack()
d_entry_text = tk.StringVar()
d_entry = tk.Entry(window, textvariable=d_entry_text)
d_entry.pack()

# Aggiungi un pulsante per processare l'immagine selezionata
process_button = tk.Button(window, text="Processa immagine", command=process_image, state=tk.DISABLED)
process_button.pack()

# Avvia la finestra di GUI
window.mainloop()